#ifndef TENSOR_H_
#define TENSOR_H_

#include "../nfgame/NFProfile.h"

namespace GTL
{
    template <class V>
    struct Tensor
    {
        std::vector<int> dimensions;
        std::vector<V> tensor;

        Tensor<V>()
        {

        }

        Tensor<V>(const std::vector<int> &Dimensions)
        {
            dimensions = Dimensions;
            tensor = std::vector<V>(product(dimensions), (V) 0);
        }

        //index function
        V& operator[](const NFProfile &profile) const
        {
            return (V&) tensor[profile.index];
        }
    };

    /*
        returns the expected value of the payoff tensor given the probability
        of each outcome occuring as given in the probabilities tensor.
    */
    template <class V, class P>
    P sdot(const Tensor<P> &probabilities, const Tensor<V> &payoffs)
    {
        P ans = P(0);
        for(int i=0; i<(int)probabilities.tensor.size(); i++)
            ans += probabilities.tensor[i] * P(payoffs.tensor[i]);
        return ans;
    }

    /*
        returns the expected value of each payoff tensor given the probability
        of each outcome occuring as given in the probabilities tensor.
    */
    template <class V, class P>
    std::vector<P> smdot(const Tensor<P> &probabilities, const std::vector<Tensor<V> > &payoffs)
    {
        std::vector<P> ans(probabilities.dimensions.size(), P(0));
        for(int i=0; i<(int)probabilities.tensor.size(); i++)
            for(int p=0; p<(int)probabilities.dimensions.size(); p++)
                ans[p] += probabilities.tensor[i] * P(payoffs[p].tensor[i]);
        return ans;
    }
}

#endif //TENSOR_H_
