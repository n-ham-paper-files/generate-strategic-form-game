#ifndef BIJECTION_H_
#define BIJECTION_H_

#include "../Const.h"

namespace GTL
{
    /*
        Struct for implementing bijections between equally sized sets
    */
    struct Bijection
    {
        int n;
        std::vector<int> bijection;

        //constructors
        Bijection()
        {

        }

        Bijection(int N)
        {
            n = N;
            reset();
        }

        Bijection(const std::vector<int> &Bijection)
        {
            set(Bijection);
        }

        //index function
        int& operator[](int i) const
        {
            return (int&) bijection[i];
        }

        //set function
        void set(const std::vector<int> &Bijection)
        {
            bijection = Bijection;
            n = bijection.size();
        }

        //sets the bijection to the identity
        void reset()
        {
            bijection = std::vector<int>(n, 0);
            for(int i=0; i<n; i++)
                bijection[i] = i;
        }

        //increments to the next bijection lexicographically
        Bijection operator++()
        {
            for(int k=n-2; k>=0; k--)
                if(bijection[k] < bijection[k+1])
                    for(int l=n-1; l>k; l--)
                        if(bijection[k] < bijection[l])
                        {
                            swap(bijection[k], bijection[l]);

                            for(int i=k+1, j=n-1; i<=j; i++, j--)
                                swap(bijection[i], bijection[j]);

                            return *this;
                        }

            //last bijection, resets to identity
            reset();

            return *this;
        }

        Bijection operator++(int)
        {
            return ++(*this);
        }

        bool operator<(const Bijection &bijection2) const
        {
            for(int i=0; i<(int)bijection.size(); i++)
            {
                if(bijection[i] > bijection2[i])
                    return 0;
                else if(bijection[i] < bijection2[i])
                    return 1;
            }
            return 0;
        }

        bool operator>(const Bijection &bijection2) const
        {
            for(int i=0; i<(int)bijection.size(); i++)
            {
                if(bijection[i] > bijection2[i])
                    return 1;
                else if(bijection[i] < bijection2[i])
                    return 0;
            }
            return 0;
        }
    };

    bool operator!=(const Bijection &bijection1, const Bijection &bijection2)     //comparison operator
    {
        if(bijection1.n != bijection2.n)
            return 1;

        for(int p=0; p<bijection1.n; p++)
            if(bijection1[p] != bijection2[p])
                return 1;

        return 0;
    }

    Bijection inverse(const Bijection &bijection)                                     //inverse operation
    {
        Bijection iPermutation(bijection.n);

        for(int p=0; p<bijection.n; p++)
            iPermutation[bijection[p] ] = p;

        return iPermutation;
    }

    Bijection operator*(const Bijection &bijection2, const Bijection &bijection1) //composition operation
    {
        Bijection nPermutation(bijection1.n);

        for(int p=0; p<(int)nPermutation.n; p++)
            nPermutation[p] = bijection2[bijection1[p] ];

        return nPermutation;
    }

    std::istream& operator>>(std::istream &is, Bijection &bijection)          //input function
    {
        for(int i=0; i<bijection.n; i++)
            is >> bijection[i];

        return is;
    }

    std::ostream& operator<<(std::ostream &os, const Bijection &bijection)    //output function
    {
        os << join(" ", bijection.bijection);
        return os;
    }
}

#endif //BIJECTION_H_
