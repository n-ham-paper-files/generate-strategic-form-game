CC=g++
CXXFLAGS=-O3 -std=c++11 -Wall -Wextra -pedantic

default: construct_game 

construct_game: construct_game.cc *.h maths/* nfgame/*
	$(CC) $(CXXFLAGS) -o construct_game construct_game.cc

clean:
	rm -f construct_game construct_game.o output.txt

run:
	./construct_game

test:
	./construct_game

