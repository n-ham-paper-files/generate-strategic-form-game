#include "GTL.h"

using namespace GTL;

int main()
{
    int noPlayers = 3;
    std::vector<int> dimensions(3, 2);

    NFGame<int, double> constructed_game(noPlayers, 2);
    constructed_game.name = "game constructed using input bijections";

    //

    NFBijection bij;
    std::vector<NFBijection> bijections;

    bij.dimensions = dimensions;
    bij.playersBijection = NFPlayersBijection(noPlayers);
    for(int p=0; p<noPlayers; p++)
        bij.strategyBijections.push_back(Bijection(dimensions[p]));

    std::ifstream ifs("./in_bijections.txt");

    while(ifs >> bij)
        bijections.push_back(bij);

    ifs.close();

    //

    make_game(constructed_game, bijections);

    std::cout << print_formatted(constructed_game) << std::endl;

    //

    std::ofstream ofs("output.txt");

    ofs << print_LaTeX(constructed_game) << std::endl;

    ofs.close();

    return 0;
}
