#ifndef GTL_H_
#define GTL_H_

/*
    Base header file for GTL.
*/

//maths
#include "./maths/Tensor.h"

//games
#include "./nfgame/NFGame.h"

//game algorithms
#include "./nfgame/NFAlgorithms.h"

#endif //GTL_H_
