Code for generating finite strategic-form games
=====

This project contains code that takes game bijections as input then constructs a finite strategic-form game that is automorphic under the inputted game bijections. The code was used to construct examples from the paper [Notions of Symmetry for Finite Strategic-Form Games](https://arxiv.org/abs/1311.4766) by Nicholas Ham. 

### Installation
In your terminal of choice, perform the following steps to install this project:

1. Clone this repository by entering 

        git clone https://gitlab.com/n-ham-paper-files/generate-strategic-form-game
      
    This will create a directory called `generate-strategic-form-game` in whatever directory you were in to begin with.
2. Type `cd generate-strategic-form-game` to change directory into the `generate-strategic-form-game` directory created in Step 1.
3. Type `make` which will create the executable `construct_game`. This project is written in C++11, so you will require a C++ compiler compatible with this standard.

You might want to test that everything worked by entering `make test` which should display:

	./construct_game
	no bijections: 6
	game constructed using input bijections
	row player: 1
	col player: 2
	Other players strategies: a,_,_
	   e      f
	c  1,2,3  4,4,4
	d  3,1,2  2,3,1
	Other players strategies: b,_,_
	   e      f
	c  2,3,1  3,1,2
	d  4,4,4  1,2,3
    
but nothing further, though it will also create the file `output.txt` that will contain $`\LaTeX`$ code for the above game, see the [$`\LaTeX`$ code saved to `output.txt`](#latex-code-saved-to-outputtxt) section below for more information.

-----

### Sample `in_bijections.txt` file
The `in_bijections.txt` file in the `generate-strategic-form-game` directory contains the input game bijections that the constructed game will be automorphic under, it is formatted as follows:

    playerPermutation
    player1StrategySetBijection
	...
    playerNStrategySetBijection

where: 
* `playersPermutation` is a permutation of the players, eg. `1 2 0` to have player 0 mapped to player 1, player 1 mapped to player 2 and player 2 mapped to player 0;
* `playerIStrategySetBijection` is the bijection from player I's strategies to the strategies for the player mapped to by I using `playersPermutation`, eg. `1 0` to have player I's first strategy map to the second strategy of the player they map to, and have player I's second strategy map to the first strategy of the player they map to.

You can include as many game bijections as you like, though they should all be for games of the same dimensions, and players that map to one another of course need to have the same number of strategies. If you want to use different dimensions you will also need to change the values of the `noPlayers` and `dimensions` variables in `construct_game.cc` and run `make` again. 

An example `in_bijections.txt` file is:

	1 2 0
	1 0
	0 1
	0 1

	2 0 1
	0 1
	1 0
	0 1

-----

### $`\LaTeX`$ code saved to `output.txt`
`construct_game` will save the $`\LaTeX`$ code for the constructed game to `output.txt`. The `generate-strategic-form-game` directory contains an example file `example.tex` which the TikZ code can be copied in to then compiled. If you would like to copy the $`\LaTeX`$ code in to your own `.tex` files you will need to include:

1. the `sgamevar.sty` $`\LaTeX`$ style file, located in the `generate-strategic-form-game` directory or available from the author Martin Osborne [here](https://www.economics.utoronto.ca/osborne/latex/); and
2. `\usepackage{sgamevar}` in the preamble of your main `.tex` file.

-----

Enjoy!

Copyright (C) 2018 Nicholas C. Ham

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="http://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.

