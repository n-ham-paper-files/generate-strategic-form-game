#ifndef NFMPROFILE_H_
#define NFMPROFILE_H_

#include "../Const.h"
#include "../maths/Tensor.h"

//mixed strategies for normal form games
namespace GTL
{
    template <class P>
    struct NFMProfile
    {
        /*
            variables
        */
        std::vector<std::vector<P> > mstrategy;
        Tensor<P> pr;

        /*
            functions
        */
        //constructors
        NFMProfile<P>(const std::vector<int> &Dimensions)
        {
            pr.dimensions = Dimensions;

            reset();
        }

        NFMProfile<P>(const std::vector<int> &Dimensions, const std::vector<std::vector<P> > &Strategy)
        {
            pr.dimensions = Dimensions;
            mstrategy = Strategy;

            formp();
        }

        //index function
        std::vector<P>& operator[](int player) const
        {
            return (std::vector<P>&) mstrategy[player];
        }

        //set functions
        void set(const std::vector<std::vector<P> > &Strategy)
        {
            mstrategy = Strategy;

            formp();
        }

        //sets the mixed strategy so each player plays each strategy with equal weights
        void reset()
        {
            mstrategy.clear();
            for(int i=0; i<(int)pr.dimensions.size(); i++)
                mstrategy.push_back(std::vector<P>(pr.dimensions[i], (P)1/(P)pr.dimensions[i]));

            formp();
        }


       //forms the probability tensor from the players mixed strategies
        /*
            The tensor formed gives the probability of each outcome occuring
            when each player plays according to their specified mixed strategy.
        */
        void formp()
        {
            NFProfile profile(pr.dimensions);
            int noProfiles = profile.noProfiles();
            pr.tensor = std::vector<P>(noProfiles, 1);

            for(int s=0; s<noProfiles; s++, profile++)
                for(int p=0; p<(int)pr.dimensions.size(); p++)
                    pr[profile] *= mstrategy[p][profile[p] ];
        }

    };

    //input function
    template <class P>
    std::istream& operator>>(std::istream &is, NFMProfile<P> &profile)
    {
        //reads each players mixed strategy
        for(int p=0; p<(int)profile.pr.dimensions.size(); p++)
            for(int a=0; a<profile.pr.dimensions[p]; a++)
                is >> profile[p][a];

        //calculates the probability of each pure strategy happening
        profile.formp();

        return is;
    }

    //output function
    template <class P>
    std::ostream& operator<<(std::ostream &os, const NFMProfile<P> &profile)
    {
        if(profile.mstrategy.size() > 0)
            os << join(' ', profile.mstrategy[0]) << std::endl;
        for(int i=1; i<(int)profile.mstrategy.size(); i++)
            os << join(' ', profile.mstrategy[i]) << std::endl;

        return os;
    }

}

#endif //NFMPROFILE_H_
