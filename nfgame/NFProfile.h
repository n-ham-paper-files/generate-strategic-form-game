#ifndef NFPROFILE_H_
#define NFPROFILE_H_

#include "../Const.h"

//mixed strategies for normal form games
namespace GTL
{
    struct NFProfile
    {
        /*
            variables
        */
        int index;                       //tensor index
        std::vector<int> dimensions;     //game dimensions
        std::vector<int> profile;       //strategy choices

        /*
            functions
        */
        //constructors
        NFProfile(const std::vector<int> &Dimensions)
        {
            index = 0;
            dimensions = Dimensions;
            profile = std::vector<int>(dimensions.size(), 0);
        }

        NFProfile(const std::vector<int> &Dimensions, const std::vector<int> &Profile)
        {
            dimensions = Dimensions;
            profile = Profile;
            calculateIndex();
        }

        //index functions
        int& operator[](int player) const
        {
            return (int&) profile[player];
        }

        void calculateIndex()
        {
            int translate = 1;
            index = 0;

            for(int p=profile.size()-1; p>=0; p--)
            {
                index += profile[p]*translate;
                translate *= dimensions[p];
            }
        }

        //set functions
        void set(const std::vector<int> &Profile)
        {
            profile = Profile;
            calculateIndex();
        }

        void set(std::vector<std::list<int>::iterator> &Profile)
        {
            for(int p=0; p<(int)profile.size(); p++)
                profile[p] = *Profile[p];
            calculateIndex();
        }

        void reset()
        {
            index = 0;
            profile = std::vector<int>(dimensions.size(), 0);
        }

        //number of strategies functions
        int noProfilesExc(int excPlayer)
        {
            int profiles = 1;

            for(int p=0; p<(int)dimensions.size(); p++)
                if(p != excPlayer)
                    profiles *= dimensions[p];

            return profiles;
        }

        int noProfilesExc(const std::vector<int> &excPlayers)
        {
            int profiles = 1;
            std::vector<bool> include(dimensions.size(),1);

            for(int p=0; p<(int)excPlayers.size(); p++)
                include[excPlayers[p] ] = 0;

            for(int p=0; p<(int)dimensions.size(); p++)
                if(include[p])
                    profiles *= dimensions[p];

            return profiles;
        }

        int noProfilesInc(int incPlayer)
        {
            return dimensions[incPlayer];
        }

        int noProfilesInc(const std::vector<int> &incPlayers)
        {
            int profiles = 1;
            std::vector<bool> include(dimensions.size(), 0);

            for(int p=0; p<(int)incPlayers.size(); p++)
                include[incPlayers[p] ] = 1;

            for(int p=0; p<(int)dimensions.size(); p++)
                if(include[p])
                    profiles *= dimensions[p];

            return profiles;
        }

        int noProfiles()
        {
            return product(dimensions);
        }

        //increment functions
        void ppExc(int excPlayer)
        {
            for(int p=dimensions.size()-1, power = 1; p>=0; power*=dimensions[p--])
            {
                if(p != excPlayer)
                {
                    if(++profile[p] != dimensions[p])
                    {
                        index += power;
                        break;
                    }
                    profile[p] = 0;
                    index -= (dimensions[p]-1)*power;
                }
            }
        }

        void ppExc(const std::vector<int> &excPlayers)
        {
            std::vector<bool> include(profile.size(), 1);
            for(int i=0; i<(int)excPlayers.size(); i++)
                include[excPlayers[i] ] = 0;

            for(int p=dimensions.size()-1, power=1; p>=0;power*=dimensions[p--])
            {
                if(include[p])
                {
                    if(++profile[p] != dimensions[p])
                    {
                        index += power;
                        break;
                    }
                    profile[p] = 0;
                    index -= (dimensions[p]-1)*power;
                }
            }
        }

        void ppInc(int incPlayer)
        {
            int power = 1;
            for(int p=dimensions.size()-1; p>incPlayer; p--)
                power *= dimensions[p];

            if(profile[incPlayer] == dimensions[incPlayer]-1)
            {
                index -= profile[incPlayer]*power;
                profile[incPlayer] = 0;
            }
            else
            {
                index += power;
                profile[incPlayer]++;
            }
        }

        void ppInc(const std::vector<int> &incPlayers)
        {
            std::vector<bool> include(profile.size(), 0);
            for(int p=0; p<(int)incPlayers.size(); p++)
                include[incPlayers[p] ] = 1;

            for(int p=dimensions.size()-1, power=1; p>=0; power*=dimensions[p--])
            {
                if(include[p])
                {
                    if(++profile[p] != dimensions[p])
                    {
                        index += power;
                        break;
                    }
                    profile[p] = 0;
                    index -= (dimensions[p]-1)*power;
                }
            }
        }

        void operator++()
        {
            for(int p=dimensions.size()-1, power=1; p>=0; power*=dimensions[p--])
            {
                if(++profile[p] != dimensions[p])
                {
                    index += power;
                    break;
                }
                profile[p] = 0;
                index -= (dimensions[p]-1)*power;
            }
        }

        void operator++(int)
        {
            ++(*this);
        }


        bool operator<(const NFProfile &profile2) const
        {
            for(int p=0; p<(int)profile.size(); p++)
            {
                if(profile[p] > profile2[p])
                    return 0;
                else if(profile[p] < profile2[p])
                    return 1;
            }
            return 0;
        }

        bool operator!=(const NFProfile &profile2) const
        {
            for(int p=0; p<(int)profile.size(); p++)
                if(profile[p] != profile2[p])
                    return 1;
            return 0;
        }

        bool operator==(const NFProfile &profile2) const
        {
            for(int p=0; p<(int)profile.size(); p++)
                if(profile[p] != profile2[p])
                    return 0;
            return 1;
        }

    };

    std::istream& operator>>(std::istream &is, NFProfile &profile)               //input function
    {
        int cStrategyChoice;
        //gets the pure strategy choices from the input stream for the no of players in the game
        profile.profile.clear();
        for(int p=0; p<(int)profile.dimensions.size(); p++)
        {
            is >> cStrategyChoice;
            profile.profile.push_back(cStrategyChoice);
        }

        //updates the index value
        profile.calculateIndex();

        return is;
    }

    std::ostream& operator<<(std::ostream &os, const NFProfile &profile)         //output function
    {
        os << join(' ', profile.profile);

        return os;
    }
}

#endif //NFPROFILE_H_
