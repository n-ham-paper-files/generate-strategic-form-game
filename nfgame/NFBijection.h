#ifndef NFBIJECTION_H_
#define NFBIJECTION_H_

#include "../Const.h"
#include "../maths/Bijection.h"
#include "NFPlayersBijection.h"
#include "NFGame.h"

//bijections for normal form games
namespace GTL
{
    template <class U, class V> struct NGame;

    struct NFBijection
    {
        /*
            variables
        */
        std::vector<int> dimensions;
        NFPlayersBijection playersBijection;
        std::vector<Bijection> strategyBijections;

        /*
            functions
        */
        //constructors
        NFBijection()
        {

        }

        template <class U, class P>
        NFBijection(const NGame<U, P> &game1, const NGame<U, P> &game2)
        {
            dimensions = game1.dimensions;
            playersBijection = NPlayersBijection(game1, game2);
            for(int p=0; p<(int)dimensions.size(); p++)
                strategyBijections.push_back(Bijection(dimensions[p]));
        }

        //iterators
        NFBijection operator++()
        {
            for(int p=dimensions.size()-1; p>=0; p--)
            {
                strategyBijections[p]++;

                if(strategyBijections[p] != Bijection(dimensions[p]))
                    return *this;
            }

            playersBijection++;

            return *this;
        }

        NFBijection operator++(int)
        {
            return ++(*this);
        }

        bool operator<(const NFBijection &bijection2) const
        {
            if(playersBijection < bijection2.playersBijection)
                return 1;
            else if(playersBijection > bijection2.playersBijection)
                return 0;

            for(int p=0; p<(int)strategyBijections.size(); p++)
            {
                if(strategyBijections[p] < bijection2.strategyBijections[p])
                    return 1;
                else if(strategyBijections[p] > bijection2.strategyBijections[p])
                    return 0;
            }

            return 0;
        }
    };

    NFBijection inverse(const NFBijection &bijection)                                        //inverse operation
    {
        NFBijection invB = bijection;

        for(int p=0; p<(int)bijection.dimensions.size(); p++)
            invB.dimensions[bijection.playersBijection[p] ] = bijection.dimensions[p];

        invB.playersBijection = inverse(bijection.playersBijection);

        for(int p=0; p<(int)bijection.playersBijection.n; p++)
            invB.strategyBijections[bijection.playersBijection[p] ] = inverse(bijection.strategyBijections[p]);

        return invB;
    }

    NFBijection operator*(const NFBijection &bijection2, const NFBijection &bijection1)       //composite operation
    {
        NFBijection cBijection = bijection1;

        cBijection.playersBijection = bijection2.playersBijection*bijection1.playersBijection;

        for(int p=0; p<(int)cBijection.playersBijection.n; p++)
            cBijection.strategyBijections[p] = bijection2.strategyBijections[bijection1.playersBijection[p] ] * bijection1.strategyBijections[p];

        return cBijection;
    }

    int operator*(const NFBijection &bijection, const int &p)                               //player action
    {
        return bijection.playersBijection[p];
    }

    NFProfile operator*(const NFBijection &bijection, const NFProfile &strategy)              //profile action
    {
        std::vector<int> nDimensions = bijection.dimensions;
        for(int p=0; p<(int)nDimensions.size(); p++)
            nDimensions[bijection.playersBijection[p] ] = bijection.dimensions[p];

        NFProfile nProfile(nDimensions);
        for(int p=0; p<(int)nDimensions.size(); p++)
            nProfile[bijection.playersBijection[p] ] = bijection.strategyBijections[p][strategy[p] ];
        nProfile.calculateIndex();

        return nProfile;
    }

    template <class U, class P>
    NGame<U, P> operator*(const NFBijection &bijection, const NGame<U, P> &game)
    {
        NGame<U, P> nGame = game;

        NFProfile profile(game.dimensions);
        for(int s=0, noProfiles=profile.noProfiles(); s<noProfiles; s++, profile++)
        {
            for(int p=0; p<game.noPlayers; p++)
                nGame.u(bijection*p, bijection*profile) = game.u(p, profile);
        }

        return nGame;
    }

    std::istream& operator>>(std::istream &is, NFBijection &bijection)               //input function
    {
        for(int p=0; p<(int)bijection.playersBijection.n; p++)
            is >> bijection.playersBijection[p];

        for(int p=0; p<(int)bijection.playersBijection.n; p++)
            for(int s=0; s<(int)bijection.strategyBijections[p].n; s++)
                is >> bijection.strategyBijections[p][s];

        return is;
    }

    std::ostream& operator<<(std::ostream &os, const NFBijection &bijection)         //output function
    {
        os << "playersBijection:";
        for(int p=0; p<(int)bijection.playersBijection.n; p++)
            os << " " << bijection.playersBijection[p];
        os << std::endl;

        for(int p=0; p<(int)bijection.playersBijection.n; p++)
        {
            os << "strategyBijections" << p << ":";
            for(int s=0; s<(int)bijection.strategyBijections[p].n; s++)
                os << " " << bijection.strategyBijections[p][s];
            os << std::endl;
        }

        return os;
    }
}

#endif //NFBIJECTION_H_
