#ifndef NFALGORITHMS_H_
#define NFALGORITHMS_H_

#include "../Const.h"
#include "NFGame.h"
#include "NFBijection.h"
#include "NFPlayersBijection.h"

/*
    This file contains algorithms for calculating properties of finite
    n-player normal form games.
*/

namespace GTL
{
    //returns all pure strategy Nash equilibria of the given game
    template <class U, class P>
    std::vector<NFProfile> pure_strategy_Nash_equilibria(NFGame<U, P> &game)
    {
        Tensor<int> maxCount(game.dimensions);
        NFProfile profile(game.dimensions);
        std::vector<NFProfile> NE;
        U uMax;

        //goes through each of the players in the game
        for(int p=0; p<game.noPlayers; p++)
        {
            //for each possible strategy profile combination excluding those of player p
            for(int s=0, noProfiles=profile.noProfilesExc(p); s<noProfiles; s++)
            {
                //finds highest attainable utility for player n given the other strategies
                uMax = game.u(p, profile);
                profile.ppInc(p);
                for(int a=1; a<game.dimensions[p]; a++)
                {
                    uMax = std::max(uMax, game.u(p, profile));
                    profile.ppInc(p);
                }

                //finds the strategies that give player p uMax
                for(int a=0; a<game.dimensions[p]; a++)
                {
                    if(game.u(p, profile) == uMax)
                    {
                        maxCount[profile]++;

                        if(maxCount[profile] == game.noPlayers) //profile is a psne
                            NE.push_back(profile);
                    }
                    profile.ppInc(p);
                }
                profile.ppExc(p);
            }
        }

        return NE;
    }

    //returns all mixed strategy Nash equilibria of the given game
    template <class U, class P>
    std::vector<NFMProfile<U> > mixed_strategy_Nash_equilibria(NFGame<U, P> &game); //haha, good luck writing this

    //returns a new game after iteratively removing pure strategies strictly dominated by a pure strategy
    template <class U, class P>
    NFGame<U, P> pure_iterated_elimination_of_strictly_dominated_pure_strategies(NFGame<U, P> &game)
    {
        NFProfile profilei(game.dimensions),
                  profilej(game.dimensions);

        std::vector<std::list<int> > strategies(game.noPlayers, std::list<int>());
        std::list<int>::iterator it_i, it_j;
        std::vector<std::list<int>::iterator> its(game.noPlayers, std::list<int>::iterator());

        //starts with all strategies as not dominated
        for(int p=0; p<game.noPlayers; p++)
            for(int a=0; a<game.dimensions[p]; a++)
                strategies[p].push_back(a);


        //iteratively finds strictly dominated strategies
        bool changed = 1;
        while(changed)
        {
            changed = 0;

            for(int p=0; p<game.noPlayers; p++)
            {
                //finds dominated strategies
                for(it_i = strategies[p].begin(); it_i != --strategies[p].end(); )
                {
                    for(it_j = (++it_i)--; it_j != strategies[p].end();)
                    {
                        bool strictlyDominatedi = 1,
                             strictlyDominatedj = 1;

                        //checks each of the other players stragies
                        int noStrategies = 1;
                        for(int p2=0; p2<game.noPlayers; p2++)
                        {
                            its[p2] = strategies[p2].begin();
                            if(p2 != p)
                                noStrategies *= strategies[p2].size();
                        }
                        for(int s=0; s<noStrategies; s++)
                        {
                            its[p] = it_i;
                            profilei.set(its);
                            its[p] = it_j;
                            profilej.set(its);

                            if(game.u(p, profilei) >= game.u(p, profilej))
                                strictlyDominatedi = 0;
                            if(game.u(p, profilej) >= game.u(p, profilei))
                                strictlyDominatedj = 0;

                            if(!strictlyDominatedi && !strictlyDominatedj)
                                break;

                            //increments the iterators
                            for(int p2=game.noPlayers-1; p2>=0; p2--)
                            {
                                if(p2 != p)
                                {
                                    if(++its[p2] != strategies[p2].end())
                                        break;
                                    its[p2] = strategies[p2].begin();
                                }
                            }
                        }

                        //checks to see if one of the two strategies is dominated
                        if(strictlyDominatedi)
                        {
                            //std::cout << "deleted " << *it_i << " for player " << p << std::endl;
                            it_i = strategies[p].erase(it_i);
                            changed = 1;
                            break;
                        }
                        else if(strictlyDominatedj)
                        {
                            //std::cout << "deleted " << *it_j << " for player " << p << std::endl;
                            it_j = strategies[p].erase(it_j);
                            changed = 1;
                        }
                        else if(++it_j == strategies[p].end())
                            ++it_i;
                    }
                }
            }
        }
        return game.subGame(strategies);
    }

    /*
        Returns a new game after iteratively removing pure strategies that are weakly dominated
        by another pure strategy.

        The result of this is NOT independent of the order in which strategies are eliminated.
    */
    template <class U, class P>
    NFGame<U, P> pure_iterated_elimination_of_weakly_dominated_pure_strategies(NFGame<U, P> &game)
    {
        NFProfile profilei(game.dimensions),
                  profilej(game.dimensions);

        std::vector<std::list<int> > strategies(game.noPlayers, std::list<int>());
        std::list<int>::iterator it_i, it_j;
        std::vector<std::list<int>::iterator> its(game.noPlayers, std::list<int>::iterator());

        //starts with all strategies as not dominated
        for(int p=0; p<game.noPlayers; p++)
            for(int a=0; a<game.dimensions[p]; a++)
                strategies[p].push_back(a);


        //iteratively finds weakly dominated strategies
        bool changed = 1;
        while(changed)
        {
            changed = 0;

            for(int p=0; p<game.noPlayers; p++)
            {
                //finds dominated strategies
                for(it_i = strategies[p].begin(); it_i != --strategies[p].end(); )
                {
                    for(it_j = (++it_i)--; it_j != strategies[p].end();)
                    {
                        bool weaklyDominatedi = 1,
                             weaklyDominatedj = 1,
                             strictFoundi = 0,
                             strictFoundj = 0;

                        //checks each of the other players stragies
                        int noStrategies = 1;
                        for(int p2=0; p2<game.noPlayers; p2++)
                        {
                            its[p2] = strategies[p2].begin();
                            if(p2 != p)
                                noStrategies *= strategies[p2].size();
                        }
                        for(int s=0; s<noStrategies; s++)
                        {
                            its[p] = it_i;
                            profilei.set(its);
                            its[p] = it_j;
                            profilej.set(its);

                            if(game.u(p, profilei) > game.u(p, profilej))
                            {
                                strictFoundi = 1;
                                weaklyDominatedi = 0;
                            }
                            else if(game.u(p, profilej) > game.u(p, profilei))
                            {
                                strictFoundj = 1;
                                weaklyDominatedj = 0;
                            }

                            if(!weaklyDominatedi && !weaklyDominatedj)
                                break;

                            //increments the iterators
                            for(int p2=game.noPlayers-1; p2>=0; p2--)
                            {
                                if(p2 != p)
                                {
                                    if(++its[p2] != strategies[p2].end())
                                        break;
                                    its[p2] = strategies[p2].begin();
                                }
                            }
                        }

                        if(weaklyDominatedi && !strictFoundj)
                            weaklyDominatedi = 0;
                        if(weaklyDominatedj && !strictFoundi)
                            weaklyDominatedj = 0;

                        //checks to see if one of the two strategies is weakly dominated
                        if(weaklyDominatedi)
                        {
                            //std::cout << "deleted " << *it_i << " for player " << p << std::endl;
                            it_i = strategies[p].erase(it_i);
                            changed = 1;
                            break;
                        }
                        else if(weaklyDominatedj)
                        {
                            //std::cout << "deleted " << *it_j << " for player " << p << std::endl;
                            it_j = strategies[p].erase(it_j);
                            changed = 1;
                        }
                        else if(++it_j == strategies[p].end())
                            ++it_i;
                    }
                }
            }
        }
        return game.subGame(strategies);
    }

    template <class U, class P>
    std::vector<std::vector<int> > partitionPlayers(const NFGame<U, P> &game)
    {
        int cStrategies = 0,
            i = -1;
        std::pair<int, int> cPair;
        std::priority_queue<std::pair<int, int>, std::vector<std::pair<int, int> >, std::greater<std::pair<int, int> > > prique;
        std::vector<std::vector<int> > playersPartition;

        for(int p=0; p<game.noPlayers; p++)
            prique.push(std::pair<int, int>(game.dimensions[p], p));

        while(!prique.empty())
        {
            cPair = prique.top();
            prique.pop();

            if(cStrategies != cPair.first)
            {
                playersPartition.push_back(std::vector<int>());
                cStrategies = cPair.first;
                i++;
            }

            playersPartition[i].push_back(cPair.second);
        }

        return playersPartition;
    }


    template <class U, class P>
    long long int no_bijections(const NFGame<U, P> &game1, const NFGame<U, P> &game2)
    {
        if(!is_bijective(game1, game2))
            return 0;

        long long int ans = 1;
        std::vector<std::vector<int> > playersPartition1 = partitionPlayers(game1);

        for(int e=0; e<(int)playersPartition1.size(); e++)
        {
            ans *= factorial((long long int) playersPartition1[e].size());
            ans *= product(std::vector<long long int>(playersPartition1[e].size(), factorial((long long int) game1.dimensions[playersPartition1[e][0] ])));
        }

        return ans;
    }

    //finds the subgroup of the automorphisms generated by gBijections
    std::vector<NFBijection> generate_bijections(const std::vector<NFBijection> &gBijections)
    {
        std::set<NFBijection> addedSet;
        std::vector<NFBijection> bijections;
        std::queue<NFBijection> checkQueue;

        for(int b=0; b<(int)gBijections.size(); b++)
        {
            addedSet.insert(gBijections[b]);
            bijections.push_back(gBijections[b]);
            checkQueue.push(gBijections[b]);
        }


        NFBijection cBijection, nBijection;
        while(!checkQueue.empty())
        {
            cBijection = checkQueue.front();
            checkQueue.pop();

            for(int b=0; b<(int)bijections.size(); b++)
            {
                nBijection = cBijection * bijections[b];
                if(!addedSet.count(nBijection))
                {
                    addedSet.insert(nBijection);
                    bijections.push_back(nBijection);
                    checkQueue.push(nBijection);
                }

                nBijection = bijections[b] * cBijection;
                if(!addedSet.count(nBijection))
                {
                    addedSet.insert(nBijection);
                    bijections.push_back(nBijection);
                    checkQueue.push(nBijection);
                }

                nBijection = inverse(nBijection);
                if(!addedSet.count(nBijection))
                {
                    addedSet.insert(nBijection);
                    bijections.push_back(nBijection);
                    checkQueue.push(nBijection);
                }
            }
        }

        return bijections;
    }

    //makes a game which is "symmetric" under the automorphisms in the subgroup generated by bijections
    template <class U, class P>
    void make_game(NFGame<U, P> &game, std::vector<NFBijection> bijections)
    {
        NFProfile profile(game.dimensions);
        bijections = generate_bijections(bijections);

        std::cout << "no bijections: " << bijections.size() << std::endl;

        //for(int n=0; n<(int)bijections.size(); n++)
          //  std::cout << bijections[n] << std::endl;

        NFProfile CProfile(game.dimensions);
        NFProfile CProfile2(game.dimensions);
        std::vector<int> strats(5, 0);
        strats[1] = 1;
        CProfile.set(strats);
        std::vector<int> strats2(5, 0);
        strats2[0] = 1;
        CProfile2.set(strats2);

        //for(int b=0; b<(int)bijections.size(); b++)
          //  if(bijections[b]*2 == 2  && bijections[b]*CProfile == CProfile2)
            //    std::cout << "WPPP " << bijections[b] << std::endl;

        int cUtility = 1;
        for(int s=0, noProfiles=profile.noProfiles(); s<noProfiles; s++, profile++)
        {
            for(int player = 0; player < game.noPlayers; player++)
            {
                if(game.u(player, profile) == 0)
                {
                    std::queue<std::pair<int, NFProfile> > q;
                    std::set<std::pair<int, NFProfile> > checked;

                    q.push(std::pair<int, NFProfile>(player, profile));
                    checked.insert(std::pair<int, NFProfile>(player, profile));

                    while(!q.empty())
                    {
                        std::pair<int, NFProfile> cInfo = q.front();
                        q.pop();

                        //std::cout << "utility " << cUtility << " player " << cInfo.first << " profile " << cInfo.second << std::endl;
                        game.u(cInfo.first, cInfo.second) = cUtility;

                        for(int b=0; b<(int)bijections.size(); b++)
                        {
                            int cPlayer = bijections[b]*cInfo.first;
                            NFProfile cProfile = bijections[b]*cInfo.second;

                            if(!checked.count(std::pair<int, NFProfile>(cPlayer, cProfile)))
                            {
                                q.push(std::pair<int, NFProfile>(cPlayer, cProfile));
                                checked.insert(std::pair<int, NFProfile>(cPlayer, cProfile));
                            }
                        }
                    }
                    cUtility++;
                }
            }
        }
    }

}

#endif //NFALGORITHMS_H_

