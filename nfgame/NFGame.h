#ifndef NFGAME_H_
#define NFGAME_H_

#include "../Const.h"

#include "NFPlayer.h"
#include "NFProfile.h"
#include "NFMProfile.h"

#include "../maths/Tensor.h"

namespace GTL
{
    /*
        Struct implementing finite n-player normal form games
    */
    template <class U, class P>
    struct NFGame
    {
        /*
            variables
        */
        std::string name;                     //name of the game
        int noPlayers;                        //number of players
        std::vector<int> dimensions;          //dimensions of the game
        std::vector<NFPlayer> players;         //players in the game
        std::vector<Tensor<U> > payoffs;      //players payoff tensors

        int rowPlayer, colPlayer;

        /*
            functions
        */
        //constructers
        NFGame<U, P>()
        {
            clear();
        }

        NFGame<U, P>(const std::string &filename)
        {
            clear();
            open(filename);
        }

        NFGame<U, P>(int NoPlayers, const std::vector<int> Dimensions)
        {
            clear();
            noPlayers = NoPlayers;
            dimensions = Dimensions;
            name = noPlayers + '0';
            name += " player " + join("x", dimensions) + " 'zero' game";
            rowPlayer = noPlayers-2;
            colPlayer = noPlayers-1;
            payoffs = std::vector<Tensor<U> >(noPlayers, Tensor<U>());
            int charat=0;
            for(int p=0; p<noPlayers; p++)
            {
                players.push_back(NFPlayer(dimensions[p]));
                for(int a=0; a<dimensions[p]; a++)
                    players[p].actions[a] = 'a' + charat++;
                payoffs[p].dimensions = dimensions;
                payoffs[p].tensor = std::vector<int>(product(dimensions), 0);
            }
        }

        NFGame<U, P>(int NoPlayers, int noStrategies)
        {
            clear();
            noPlayers = NoPlayers;
            dimensions = std::vector<int>(noPlayers, noStrategies);
            name = noPlayers + '0';
            name += " player " + join("x", dimensions) + " 'zero' game";
            rowPlayer = noPlayers-2;
            colPlayer = noPlayers-1;
            payoffs = std::vector<Tensor<U> >(noPlayers, Tensor<U>());
            int charat=0;
            for(int p=0; p<noPlayers; p++)
            {
                players.push_back(NFPlayer(dimensions[p]));
                for(int a=0; a<dimensions[p]; a++)
                    players[p].actions[a] = 'a' + charat++;
                payoffs[p].dimensions = dimensions;
                payoffs[p].tensor = std::vector<int>(product(dimensions), 0);
            }
        }

        //clear function
        void clear()
        {
            name = "";
            dimensions.clear();
            players.clear();
            payoffs.clear();
        }

        //single io functions
        void open(const std::string &filename)
        {
            clear();
            std::ifstream ifs(filename.c_str());
            ifs >> *this;
            ifs.close();
        }

        void save(const std::string &filename)
        {
            std::ofstream ofs(filename.c_str());
            ofs << print_unformatted(*this);
            ofs.close();
        }

        /*
            (expected) utility functions
        */
        //returns the players payoff for the given pure strategy profile
        U& u(int player, const NFProfile &profile) const
        {
            return (U&) payoffs[player][profile];
        }

        //returns the payoffs for the given pure strategy profile
        std::vector<U> u(const NFProfile &profile) const
        {
            std::vector<U> utilities;
            for(int player=0; player<noPlayers; player++)
                utilities.push_back(payoffs[player][profile]);
            return utilities;
        }

        //returns the players expected payoff from the given mixed strategy profile
        P u(int player, const NFMProfile<P> &profile)
        {
            return sdot(profile.pr, payoffs[player]);
        }

        //returns the expected payoffs from the given mixed strategy profile
        std::vector<P> u(const NFMProfile<P> &profile)
        {
            return smdot(profile.pr, payoffs);
        }

        //returns a new game with the specified strategies to include
        NFGame<U, P> subGame(std::vector<std::list<int> > &strategies)
        {
            NFGame<U, P> newGame;
            newGame.name = name;
            newGame.noPlayers = noPlayers;
            newGame.rowPlayer = rowPlayer;
            newGame.colPlayer = colPlayer;
            newGame.players = std::vector<NFPlayer>(noPlayers, NFPlayer(0));

            std::vector<std::list<int>::iterator> its(noPlayers, std::list<int>::iterator());

            for(int p=0; p<noPlayers; p++)
            {
                newGame.dimensions.push_back(strategies[p].size());
                newGame.players[p].noActions = newGame.dimensions[p];

                for(its[p] = strategies[p].begin(); its[p]!=strategies[p].end(); its[p]++)
                    newGame.players[p].actions.push_back(players[p].actions[*its[p] ]);

                its[p] = strategies[p].begin();
            }

            //creates the new payoff matrix
            NFProfile oldprofile(dimensions),
                      newprofile(newGame.dimensions);
            newGame.payoffs = std::vector<Tensor<U> >(noPlayers, Tensor<U>(newGame.dimensions));
            int noProfiles = newprofile.noProfiles();
            for(int s=0; s<noProfiles; s++, newprofile++)
            {
                oldprofile.set(its);
                for(int p=0; p<noPlayers; p++)
                    newGame.payoffs[p][newprofile] = payoffs[p][oldprofile];

                //increments the iterators
                for(int p=noPlayers-1; p>=0; p--)
                {
                    if(++its[p] != strategies[p].end())
                        break;
                    its[p] = strategies[p].begin();
                }

            }

            return newGame;
        }
    };

    /*
        io functions
    */
    //input function
    template <class U, class P>
    std::istream& operator>>(std::istream &is, NFGame<U, P> &game)
    {
        game.clear();
        std::string lineType, junk;

        int p=0, t=0;
        while(is >> lineType)
        {
            if(lineType == "name")
                getline(is.ignore(1), game.name);
            else if(lineType == "players")
            {
                is >> game.noPlayers;
                game.rowPlayer = game.noPlayers-2;
                game.colPlayer = game.noPlayers-1;
            }
            else if(lineType == "dimensions")
            {
                game.dimensions = std::vector<int>(game.noPlayers, 0);
                for(int p=0; p<game.noPlayers; p++)
                {
                    is >> game.dimensions[p];
                    game.players.push_back(NPlayer(game.dimensions[p]));
                }
                game.payoffs = std::vector<Tensor<U> >(game.noPlayers, Tensor<U>(game.dimensions));
            }
            else if(lineType == "player")
                is >> game.players[p++];
            else if(lineType == "payoffs")
                is >> game.payoffs[t++].tensor;
            else if(lineType == "end")
                break;
            else
                getline(is, junk);
        }

        //creates generic information if none was provided
        if(game.name == "")
            game.name = join("x", game.dimensions) + " normal form game";
        for(; p<game.noPlayers; p++)
        {
            game.players[p].name = "p" + toString(p);
            for(int a=0; a<game.players[p].noActions; a++)
                game.players[p].actions[a] = toString(a);
        }

        return is;
    }

    //output function
    template <class U, class P>
    std::ostream& operator<<(std::ostream& os, const NFGame<U, P> &game)
    {
        os << print_unformatted(game);

        return os;
    }

    //returns the output string of the game in the requested format
    template <class U, class P>
    std::string toString(const NFGame<U, P> &game, int format)
    {
        switch(format)
        {
            case 0:
                return print_unformatted(game);
                break;
            case 1:
                return print_formatted(game);
                break;

            default:
                return print_unformatted(game);
                break;
        }
    }

    template <class U, class P>
    std::string print_unformatted(const NFGame<U, P> &game)
    {
        std::stringstream ss;

        //adds the name of the game and the number of players to the output file
        ss << "name " << game.name << std::endl;
        ss << "players " << game.noPlayers << std::endl;
        ss << "dimensions " << game.dimensions << std::endl;

        //adds each of the players names and their pure strategies to the output file
        for(int p=0; p<game.noPlayers; p++)
            ss << "player " << game.players[p] << std::endl;

        //adds each players payoff tensor values to the output file
        for(int p=0; p<game.noPlayers; p++)
            ss << "payoffs " << game.payoffs[p].tensor << std::endl;

        ss << "end" << std::endl;

        return ss.str();
    }

    template <class U, class P>
    std::string print_formatted(const NFGame<U, P> &game)
    {
        std::stringstream ss;
        ss.setf(std::ios::left);

        //adds the name of the game to the output stream
        ss << game.name << std::endl;
        ss << "row player: " << game.rowPlayer << std::endl;
        ss << "col player: " << game.colPlayer << std::endl;

        NFProfile profile(game.dimensions);
        std::vector<int> excPlayers = Vector(2, game.rowPlayer, game.colPlayer);

        //for each of the players other than rowPlayer and colPlayers strategy combinations
        int noStrategies = profile.noProfilesExc(excPlayers);
        for(int s=0; s<noStrategies; s++)
        {
            //outputs the strategies of players that aren't rowPlayer or colPlayer
            if(game.noPlayers > 2)
            {
                std::vector<std::string> otherStrategies(game.noPlayers, "");
                for(int p=0; p<game.noPlayers; p++)
                {
                    if(p == game.rowPlayer || p == game.colPlayer)
                        otherStrategies[p] = "_";
                    else
                        otherStrategies[p] = game.players[p].actions[profile[p] ];
                }

                ss << "Other players strategies: " << join(',', otherStrategies) << std::endl;
            }

            std::vector<std::vector<std::string> > output(game.dimensions[game.rowPlayer]+1,
                                                          std::vector<std::string>(game.dimensions[game.colPlayer]+1, ""));

            //gets the strategy choices of colPlayer
            for(int a1=0; a1<game.players[game.colPlayer].noActions; a1++)
                output[0][a1+1] = game.players[game.colPlayer].actions[a1];

            //gets the current lot of payoffs
            for(int a0=0; a0<game.players[game.rowPlayer].noActions; a0++)
            {
                //adds the strategy choice of rowPlayer
                output[a0+1][0] = game.players[game.rowPlayer].actions[a0];

                //adds the current row of payoffs
                for(int a1=0; a1<game.players[game.colPlayer].noActions; a1++)
                {
                    output[a0+1][a1+1] += toString(game.u(0,profile));
                    for(int p=1; p<game.noPlayers; p++)
                        output[a0+1][a1+1] += "," + toString(game.u(p,profile));
                    profile.ppInc(game.colPlayer);
                }
                profile.ppInc(game.rowPlayer);
            }
            profile.ppExc(excPlayers);

            //works out the output width of each column
            std::vector<int> colWidth(output[0].size(), 0);
            for(int r=0; r<(int)output.size(); r++)
                for(int c=0; c<(int)output[r].size(); c++)
                    colWidth[c] = std::max(colWidth[c], (int)output[r][c].size());

            //outputs the current lot of payoffs
            for(int r=0; r<(int)output.size(); r++)
            {
                for(int c=0; c<(int)output[r].size(); c++)
                {
                    ss.width(colWidth[c]);
                    ss << output[r][c] << "  ";
                }
                ss << std::endl;
            }
        }

        return ss.str();
    }

    //LaTeX output function (requires sgamevar.sty from Osborne)
    template <class U, class P>
    std::string print_LaTeX(const NFGame<U, P> &game)
    {
        std::stringstream ss;
        ss.setf(std::ios::left);

        NFProfile profile(game.dimensions);
        std::vector<int> excPlayers;
        excPlayers.push_back(game.rowPlayer);
        excPlayers.push_back(game.colPlayer);

        ss << "\\begin{center}" << std::endl;

        //for each of the players other than rowPlayer and colPlayers strategy combinations
        int noStrategies = profile.noProfilesExc(excPlayers);
        for(int s=0; s<noStrategies; s++)
        {
            ss << "  \\begin{game}{" << game.dimensions[0] << "}{" << game.dimensions[1] << "}";
            if(game.noPlayers > 2)
            {
                std::vector<std::string> otherStrategies(game.noPlayers, "");
                for(int p=0; p<game.noPlayers; p++)
                {
                    if(p == game.rowPlayer)
                        otherStrategies[p] = "\\underline{r}";
                    else if(p == game.colPlayer)
                        otherStrategies[p] = "\\underline{c}";
                    else
                        otherStrategies[p] = game.players[p].actions[profile[p] ];
                }
                ss << "[" << join(' ', otherStrategies) << "]";
            }
            ss << std::endl;

            std::vector<std::vector<std::string> > output(game.dimensions[game.rowPlayer]+1,
                                                          std::vector<std::string>(game.dimensions[game.colPlayer]+1, ""));

            //gets the strategy choices of colPlayer
            for(int a1=0; a1<game.players[game.colPlayer].noActions; a1++)
                output[0][a1+1] = game.players[game.colPlayer].actions[a1];

            //gets the current lot of payoffs
            for(int a0=0; a0<game.players[game.rowPlayer].noActions; a0++)
            {
                //adds the strategy choice of rowPlayer
                output[a0+1][0] = game.players[game.rowPlayer].actions[a0];

                //adds the current row of payoffs
                for(int a1=0; a1<game.players[game.colPlayer].noActions; a1++)
                {
                    output[a0+1][a1+1] = "$" + toString(game.u(0,profile));
                    for(int p=1; p<game.noPlayers; p++)
                        output[a0+1][a1+1] += "," + toString(game.u(p,profile));
                    output[a0+1][a1+1] += "$";
                    profile.ppInc(game.colPlayer);
                }
                profile.ppInc(game.rowPlayer);
            }
            profile.ppExc(excPlayers);

            //works out the output width of each column
            std::vector<int> colWidth(output[0].size(), 0);
            for(int r=0; r<(int)output.size(); r++)
                for(int c=0; c<(int)output[r].size(); c++)
                    colWidth[c] = std::max(colWidth[c], (int)output[r][c].size());

            //outputs the current lot of payoffs
            for(int r=0; r<(int)output.size(); r++)
            {
                ss << "    ";
                for(int c=0; c<(int)output[r].size(); c++)
                {
                    ss.width(colWidth[c]);
                    ss << output[r][c] << "  ";

                    if(c+1 < (int)output[r].size())
                        ss << "\\>  ";
                }
                if(r+1 < (int)output.size())
                    ss << "\\\\";
                ss << std::endl;
            }


            ss << "  \\end{game}" << std::endl;

            if(s+1 < noStrategies)
                ss << "  \\hspace*{5mm}" << std::endl;
        }
        ss << "\\end{center}" << std::endl;

        return ss.str();
    }

}

#endif //NFGAME_H_
