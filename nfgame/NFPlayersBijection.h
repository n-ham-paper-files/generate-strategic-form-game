#ifndef NFPLAYERSBIJECTION_H_
#define NFPLAYERSBIJECTION_H_

#include "../Const.h"

namespace GTL
{
    template <class U, class V> struct NGame;

    struct NFPlayersBijection
    {
        int n;
        std::vector<std::vector<int> > playersPartition1, playersPartition2;
        std::vector<int> playersBijection;
        std::vector<int> identity;

        //constructors
        NFPlayersBijection()
        {

        }

        NFPlayersBijection(int N)
        {
            n = N;
            playersBijection = identity = std::vector<int>(n, 0);

            for(int p=0; p<n; p++)
                playersBijection[p] = identity[p] = p;
        }

        template <class U, class P>
        NFPlayersBijection(const NGame<U, P> &game1, const NGame<U, P> &game2)
        {
            n = game1.noPlayers;
            playersPartition1 = partitionPlayers(game1);
            playersPartition2 = partitionPlayers(game2);

            identity = std::vector<int>(n, 0);

            for(int e=0; e<(int)playersPartition1.size(); e++)
                for(int p=0; p<(int)playersPartition1[e].size(); p++)
                    identity[playersPartition1[e][p] ] = playersPartition2[e][p];

            reset();
        }

        bool isPartitionIdentity(int e)
        {
            for(int p=0; p<(int)playersPartition1[e].size(); p++)
                if(playersBijection[playersPartition1[e][p] ] != identity[playersPartition1[e][p] ])
                    return 0;

            return 1;
        }

        //index function
        int& operator[](int i) const
        {
            return (int&) playersBijection[i];
        }

        //sets the permutation to the identity
        void reset()
        {
            playersBijection = identity;
        }

        //increments to the next permutation lexicographically
        NFPlayersBijection operator++()
        {
            for(int e=playersPartition1.size()-1; e >= 0; e--)
            {
                for(int k=playersPartition1[e].size()-2; k>=0; k--)
                {
                    if(playersBijection[playersPartition1[e][k] ] < playersBijection[playersPartition1[e][k+1] ])
                    {
                        for(int l=playersPartition1[e].size()-1; l>k; l--)
                        {
                            if(playersBijection[playersPartition1[e][k] ] < playersBijection[playersPartition1[e][l] ])
                            {
                                swap(playersBijection[playersPartition1[e][k] ], playersBijection[playersPartition1[e][l] ]);

                                for(int i=k+1, j=playersPartition1[e].size()-1; i<=j; i++, j--)
                                    swap(playersBijection[playersPartition1[e][i] ], playersBijection[playersPartition1[e][j] ]);

                                if(isPartitionIdentity(e))
                                    break;
                                else
                                    return *this;
                            }
                        }
                        break;
                    }
                }

                //resets the current partition to the identity
                for(int p=0; p<(int)playersPartition1[e].size(); p++)
                    playersBijection[playersPartition1[e][p] ] = identity[playersPartition1[e][p] ];
            }

            return *this;
        }

        NFPlayersBijection operator++(int)
        {
            return ++(*this);
        }

        bool operator<(const NFPlayersBijection &bijection2) const
        {
            for(int i=0; i<(int)playersBijection.size(); i++)
            {
                if(playersBijection[i] > bijection2[i])
                    return 0;
                else if(playersBijection[i] < bijection2[i])
                    return 1;
            }
            return 0;
        }

        bool operator>(const NFPlayersBijection &bijection2) const
        {
            for(int i=0; i<(int)playersBijection.size(); i++)
            {
                if(playersBijection[i] > bijection2[i])
                    return 1;
                else if(playersBijection[i] < bijection2[i])
                    return 0;
            }
            return 0;
        }
    };

    NFPlayersBijection inverse(const NFPlayersBijection &bijection)                                     //inverse operation
    {
        NFPlayersBijection iBijection = bijection;

        iBijection.playersPartition1 = bijection.playersPartition2;
        iBijection.playersPartition2 = bijection.playersPartition1;

        for(int p=0; p<bijection.n; p++)
            iBijection[bijection[p] ] = p;

        return iBijection;
    }

    NFPlayersBijection operator*(const NFPlayersBijection &bijection2, const NFPlayersBijection &bijection1) //composition operation
    {
        NFPlayersBijection nBijection = bijection1;

        nBijection.playersPartition2 = bijection2.playersPartition2;

        for(int p=0; p<(int)nBijection.n; p++)
            nBijection[p] = bijection2[bijection1[p] ];

        return nBijection;
    }


    std::istream& operator>>(std::istream &is, NFPlayersBijection &bijection)          //input function
    {
        for(int i=0; i<bijection.n; i++)
            is >> bijection[i];

        return is;
    }

    std::ostream& operator<<(std::ostream &os, const NFPlayersBijection &bijection)    //output function
    {
        os << join(" ", bijection.playersBijection);
        return os;
    }
}

#endif //NFPLAYERSBIJECTION_H_
