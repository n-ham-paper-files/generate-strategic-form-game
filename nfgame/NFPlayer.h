#ifndef NFPLAYER_H_
#define NFPLAYER_H_

#include "../Const.h"

namespace GTL
{
    /*
        This struct stores information for a player in a normal form game.
    */
    struct NFPlayer
    {
        std::string name;
        int noActions;
        std::vector<std::string> actions;

        NFPlayer(int NoActions)
        {
            noActions = NoActions;
            actions = std::vector<std::string>(noActions, "");
        }
    };

    std::istream& operator>>(std::istream &is, NFPlayer &player)        //input function
    {
        is >> player.name >> player.actions;

        return is;
    }

    std::ostream& operator<<(std::ostream &os, const NFPlayer &player)  //output function
    {
        os << player.name << " " << player.actions;

        return os;
    }
}

#endif //NFPLAYER_H_
